<?php if ($editor) { ?>
	<link rel="stylesheet" href="<?php echo HTTP_ROOT;?>assets/global/plugins/bootstrap-summernote/summernote.css">
	<script type="text/javascript" src="<?php echo HTTP_ROOT;?>assets/global/plugins/bootstrap-summernote/summernote.min.js"></script>

	<script type="text/javascript" src="<?php echo HTTP_ROOT;?>assets/global/plugins/emojionearea-master/dist/emojionearea.js"></script>

	<script type="text/javascript">
		var ComponentsEditors = function () {
			var handleSummernote = function () {
				$('.summernote').summernote({height: 300});
			}
			
			return {
				//main function to initiate the module
				init: function () {
					handleSummernote();
				}
			};
		}();
		jQuery(document).ready(function() {    
			$("#facebook-title").emojioneArea();
			$("#facebook-description").emojioneArea();
			$(".hint2emoji").emojioneArea();
			ComponentsEditors.init(); 
		});
	</script>

<?php } ?>
<link href="../css/sonoclick.css" rel="stylesheet" type="text/css" />

<link href="../assets/global/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo HTTP_ROOT; ?>assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css">
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN THEME GLOBAL STYLES -->
<link href="../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />

<!-- END THEME GLOBAL STYLES -->

<style>

	.view_form_page .container{
		max-width: 1440px;
	}

	.view_form .social_row .fb{
		text-align: right;
		border-right:none;
	}

	.view_form .social_row .you_tube, .view_form .social_row .fb{
		min-height: 100px;
		display: flex;
		align-items: center;
		margin-bottom: 12px;
	}

	.view_form .social_row img {
		max-width: 180px;
	}

	.view_form .social_row .you_tube img {
		max-width: 165px;
	}

	.fb_public .fb_border {
		padding:0
	}

	.fb_public .fb_border>.col-sm-12{
		padding:0;
	}

	.fb_public .heading span{
		color:#f19300
	}

	.fb_public .next_btn{
		color: #000000;
		padding: 6px 32px;
		font-size: 16px;
		float: right;
		margin: 15px 20px 15px 0;
		background:#f0ff00;
		text-transform: uppercase;
		font-family: "Open Sans",sans-serif;
	}

	.fb_public .next_btn:hover{
		background:#f0ff00;
	}

	.fb_public .back_btn{
		color: #000000;
		padding: 6px 32px;
		font-size: 16px;
		float: right;
		margin: 15px 20px 15px 0;
		background:#ffa200;
		text-transform:uppercase;
		font-family: "Open Sans",sans-serif;
	}

	.fb_public .back_btn:hover{
		background:#ffa200;
	}

	.fb_public img{
		max-width:100%
	}

	.youtube_official{
		color: #ff0000 !important;
	}

	#stream_description-error, #stream_title-error
	{
		color: red;
	}

	#bottom-help-img{
		z-index: 2147483646;
		width: 60px;
		height: 60px;
		font-size: 18px;
		font-weight: 400;
		overflow: hidden;
		justify-content: center;
		align-items: center;
		display: flex;
		cursor: pointer;
		position: fixed;
		right: 10px;
		bottom: 10px;
		
	}

	.col-form-comming-users{
		padding: 0 30%;
		text-align: left;
	}

	/*iPhone 6/7/8 */
	@media (max-width: 768px){

		/* Form Comming Users */
		.col-form-comming-users{
			padding: 0 15%;
		}



	}

	.page-content{
		position: relative;
	}

	.page-content .view_form_page{
		background-image: url("http://www.multiplierapp.fcdesigner.art.br/wp-content/uploads/2020/01/2303556048-BG.jpg");
		background-attachment: fixed !important;
		background-size: cover !important;
		background-position: top !important;
		background-repeat: no-repeat !important;
		position: relative;
	}

	<?php if ( $formInfo->selected_image ){ ?>

		.page-content .view_form_page{
			background-image: url(" <?=  $formInfo->selected_image ?> ");
		}

	<?php } ?>

	.page-content .view_form_page{
		margin-left: 0;
		border: 0;
		width: 100%;
	}

	.page-content-wrapper .page-content.view_form_page {
		margin-top: 0px;
	}

	.img-zoom{
		transition: 0.5s transform ease;
	}

	.img-zoom:hover{
		transform: scale(1.1);
	}

	.btn-multiplier{
		color: black;
		border: 1px #00000036 solid;
		border-radius: 5px !important;
		padding: 5px 20px;
		transition: 0.3s ease background;
		cursor: pointer;
		margin: 5px 10px;
	}

	.btn-multiplier:hover{
		background: #fcc946 !important;
		color: white;
	}

	.btn-green{
		color: white;
		background: #5daaa7 !important;
	}

	/** Css Header */

	.page-wrapper > .tutorial-text-box{
		top: 40px;
	}


	#header_row{
		padding-bottom: 40px;
		position: relative;
	}

	#header_logo_wrapper img{
		width: 50%;
	}

	#header_logo_wrapper .tutorial-text-box{
		top: 100px;
	}

	#flag_wrapper{
		position: absolute;
		right: 0;
		top: 50%;
		transform: translateY(-50%)
	}

	.view_form .flag_row{
		text-align: end;
		margin-right: 50px;
	}

	.view_form .flag_row .flag{
		text-decoration: none;
	}

	.view_form .flag_row .flag img{
		height: 40px;
		width: 45px;
		border-radius: 20px !important;
		margin: 0 10px;
	}

	.flag.active img{
		border: 2px solid #00ef00;
	}

	#background_edition{
		position: absolute;
		top: 10px;
		right: 0;
		z-index: 1;
	}

	#main_video{
		position: absolute;
		top: 0px;
		right: 0;
		z-index: 1;
	}

	/** Css Details Text */

	.row.detail_text{
		position: relative;
		background: none !important;
		margin-bottom: 15%;
	}

	#convite_text_wrapper{
		background: white;
		border-radius: 20px !important;
		padding: 50px;
		float: right;
		margin-bottom : 20px;
	}

	#convite_text_wrapper .tutorial-text-box{
		right: 30px;
	}

	#main_text{
		padding-bottom: 20px;
	}

	#main_text_edition_confirm{
		font-size: 1.5em;
		color: black;
		position: absolute;
		right: 0;
		top: 0;
		padding: 20px;
		cursor: pointer;
		transition: 0.5s ease all; 
	}

	#main_text_edition_confirm:hover{
		color: #fcc946
	}

	#invitation_headline_wrapper{
		position: absolute;
		left: 0;
		top: 50%;
		transform: translateY(-50%);
	}

	#invitation_headline_wrapper .tutorial-text-box{
		top: 50px;
	}

	#invitation_headline{
		color: white;
		font-weight: bold;
		text-shadow: 0px 0px 5px #000;
	}

	#invitation_headline > p{
		font-size: 1.5em !important;
		text-align: left;
		line-height: 1.2;
		margin-left: 50px;
	}

	#headline_edition_input{
		background: none;
		border: none;
		box-shadow: 0px 1px 0px 0px #ffffff63;
		width: 100%;
		color: white;
		text-shadow: 0px 0px 5px #000;
	}

	#headline_edition_confirm{
		font-size: 1.5em;
		color: white;
		position: absolute;
		right: 30px;
		transform: translate(50%, 50%);
		cursor: pointer;
		transition: 0.5s ease all; 
		text-shadow: 0px 0px 5px #000;
	}

	#headline_edition_confirm:hover{
		color: #fcc946;
	}

	#social_title_wraper{
		position: relative;
		min-height: 80px;
	}

	#social_title_edition_confirm{
		font-size: 1.5em;
		color: black;
		position: absolute;
		right: 0;
		transform: translate(50%, 50%);
		cursor: pointer;
		transition: 0.5s ease all; 
	}

	#social_title_edition_confirm:hover{
		color: #fcc946;
	}

	#social_title_edition_input{
		background: none;
		border: none;
		box-shadow: 0px 1px 0px 0px #00000070;
		width: 100%;
		color: black;
	}

	#initial-video{
		position: relative;
		width: 100%;
		display: inline-block;
	}

	/** Css Social Row */

	.view_form .social_row{
		border: none;
		background: white;
		border-radius: 20px !important;
		padding: 70px 100px 20px 100px;
		margin-bottom: 15%;
		clear: both;
	}

	.social_row .tutorial-text-box{
		right: 50%;
		top: 50%;
		transform: translate(50%, -50%);
	}

	#social_title{
		color: black;
		font-weight: bold;
	}

	#social_title > p{
		font-size: 1.5em !important;
		text-align: center;
		line-height: 1.2;
	}

	#social_title > p > b{
		font-weight: 600;
	}

	#social_warning{
		border: 1.5px solid #de0000;
		border-radius: 20px !important;
		padding: 20px;
		letter-spacing: 0.5px;
		line-height: 1.7;
		margin-bottom: 100px;
		text-align: left;
	}

	#col_social_text{
		padding-left: 0;
	}

	.col-form-comming-users{
		padding: 0;
	}

	.social_row > div{
		padding: 0 30px;
	}

	#social_image{
		max-width: 250px;
	}

	.choose_one p{
		text-align: left;
		font-weight: 500;
		line-height: 1.5;
		letter-spacing: 0.5px;
	}

	.logo_row hr{
		margin: 40px 0;
	}

	#share_text{
		float: left;
		margin-left: 60px;
	}

	#privacy_text_edition_confirm{
		font-size: 1.5em;
		color: black;
		position: absolute;
		right: 0;
		bottom: 50%;
		transform: translate(50%, 50%);
		cursor: pointer;
		transition: 0.5s ease all;
	}

	#privacy_text_edition_confirm:hover{
		color: #fcc946;
	}

	#countryflag{
		padding-top: 0px;
		border: 1px solid #00000036;
	}

	.button-preview-copy{
		float: right;
		margin-right: 85px;
	}

	.you_tube{
		padding-left: 20px;
	}

	.view_form .social_row .fb_border img{
		max-width: 100%;
	}

	/** Css Footer */

	.view_form{
		padding: 0;
	}

	#footer_card{
		border: none;
		background: white;
		border-radius: 20px 20px 0 0 !important;
		padding: 50px 100px 10px 100px;
	}

	#footer_logo{
		width: 55%;
	}

	.footer-tag-img{
		width: 35%;
		vertical-align: middle;
		padding: 0 2%;
	}

	.footer-tag-img{
		text-decoration: none;
	}

	#footer_wrapper{
		position: relative; 
		margin-bottom: 20px;
	}

	#footer_login_wrapper{
		text-align: end;
		margin-top: 35px;
	}

	#footer_login_wrapper a:hover{
		color: white;
	}

	#footer_login_wrapper .btn{
		margin: 0 20px;
	}

	.knowMore_row{
		margin: 20px 0 40px 0;
	}

	#app_stores_wrapper{
		margin-bottom: 25px;
	}

	#app_stores{
		display: inline-block;
		width: 50%;
	}

	#text_rights{
		margin:0;
	}

	.knowMore_row a{
		color: black;
	}

	.knowMore_row a:hover{
		color: #5daaa7
	}

	#icons{
		text-align: end;
		margin-top: 20px;
	}

	#icons a{
		color: black;
		text-decoration: none;
	}

	#icons i{
		transition: 0.5s ease color;
		color: black;
		padding: 0 10px;
		font-size: 2em;
	}

	#icons i:hover{
		color: #fcc946;
	}

	/** Css Tutorial */

	.blocked{
		z-index: 0 !important;
	}

	.tutorial-overlay{
		position: fixed;
		width: 100%;
		top: 0;
		bottom: 0;
		left: 0;
		background: black;
		z-index: 1;
		opacity: 0.65;
	}

	.tutorial-item-active{
		position: relative;
		z-index: 2;
	}

	.tutorial-overlay.tutorial-item-active{
		opacity: 0;
	}

	.tutorial-start-text-box{
		min-width: 220px;
		background: white;
		position: fixed;
		top: 150px;
		right: 50%;
		padding: 10px;
		border-radius: 5px !important;
		z-index: 4;
		transform: translate(50%, 50%);
		text-align: center;
		border: 1px solid #00000059;
		width: 60%;
	}

	.tutorial-start-text-box p{
		padding: 0 10px;
	}

	.tutorial-text-box{
		background: white;
		position: absolute;
		right: 0;
		top: 30px;
		padding: 15px !important;
		border-radius: 5px !important;
		margin-top: 15px;
		border: 1px solid #00000059;
		text-align: center;
		z-index: 2;
	}

	.tutorial-button-wraper{
		text-align: center;
	}

	.icon-editable{
		position: absolute;
		top: 0;
		right: 0;
		font-size: 1.2em;
		padding: 20px;
		cursor: pointer;
		transition: 0.3s ease all;
	}

	.icon-editable.flags{
		top: -10px;
		right: -50px;
	}

	.icon-editable:hover{
		color: #fcc946;
	}

	#remove_video{
		position: absolute;
		top: 0px;
		left: 0;
		z-index: 1;
    	right: unset;
	}

	.lateral-padding-0{
		padding-left: 0;
		padding-right: 0;
	}

	.icon-white{
		color: white;
		text-shadow: 0px 0px 5px #000;
	}

	.icon-black{
		color: black;
	}

	.editable-content{
		transition: 0.5s all ease;
	}

	.editable-content-highlight{
		transform: scale(1.1);
	}

	/** Image Select */

	#images_preview .panel-collapse .panel-body{
		max-height: 250px; 
    	overflow: auto;
		min-height: 160px;
	}

	#images_modal{
		background: white;
		position: fixed;
		top: 100px;
		right: 50%;
		padding: 10px;
		border-radius: 5px !important;
		z-index: 2;
		transform: translate(50%, 0%);
		width: 80%;
		height: 760px;
	}

	#images_modal_video{
		background: white;
		position: fixed;
		top: 100px;
		right: 50%;
		padding: 10px;
		border-radius: 5px !important;
		z-index: 2;
		transform: translate(50%, 0%);
		width: 80%;
		height: 760px;
	}

	#close_modal_images{
		position: absolute;
		right: 10px;
		top: 10px;
		font-size: 1.5em;
		cursor: pointer;
		z-index: 2;
	}

	#close_modal_images:hover{
		color: #fcc946;
	}

	#close_modal_images_video{
		position: absolute;
		right: 10px;
		top: 10px;
		font-size: 1.5em;
		cursor: pointer;
		z-index: 2;
	}

	#close_modal_images_video:hover{
		color: #fcc946;
	}

	#images_wrapper{
		height: 700px;
		padding: 10px;
	}

	#images_wrapper_video{
		height: 700px;
		padding: 10px;
	}

	#images_preview{
		position: absolute;
		top: 0;
		left: 0;
		overflow: auto;
	}

	#images_preview_video{
		position: absolute;
		top: 0;
		left: 0;
		overflow: auto;
	}

	#image_show_wrapper{
		position: absolute;
		right: 0;
		top: 0;
		bottom: 0;
		box-shadow: -1px 0px 20px #99999954;
		overflow: auto;
	}

	.select-image-item{
		position: relative;
		width: 31%;
		box-shadow: 2px 5px 5px #99999982;
		float: left;
		margin: 10px 5px;
		cursor: pointer;
	}

	.add-file-wrapper > div{
		width: 100%;
		height: 110px;
		background-color: #5daaa7;
		transition: 0.5s ease all;
	}

	.add-file-wrapper > div:hover{
		background-color: #fccd56;
	}

	.add-file{
		font-size: 3.2em;
		position: absolute;
		top: 40%;
		left: 50%;
		transform: translate(-50%);
		color: white;
	}

	.select-image-item{
		height: 90px;
		overflow: hidden;
	}

	.select-image-item img{
		width: 100%;
		height: 100%;
	}

	.select-image-item video{
		width: auto;
		height: 100%;
	}

	.delete-image-icon{
		position: absolute;
		right: 5px;
		top: 5px;
		color: white;
		cursor: pointer;
		text-shadow: 0px 0px 5px #000;
		z-index: 1;
	}

	#image_holder_wrapper{
		width: 100%;
		padding: 0 20px;
		overflow: hidden;
	}

	#image_holder_wrapper img{
		height: 60vh;
	}

	#image_holder_wrapper > div{
		max-height: 60vh
	}

	#image_holder{
		width: 100%;
	}

	#logo_holder{
		width: 150px;
		height: auto !important;

		position: absolute;
		right: 50%;
		top: 50%;
		transform: translate(50%, -50%);
	}

	#colorpicker{
		height: 90px;
	}

	#video_holder_wrapper{
		width: 100%;
		padding: 0 20px;
		overflow: hidden;
	}

	#video_holder_wrapper video{
		height: 60vh;
	}

	#video_holder_wrapper > div{
		max-height: 60vh
	}

	#preview_video_holder{
		width: 100%;
	}

	#initial_video_placeholder{
		width: 100%;
		border: 2px dashed #5daaa7;
		height: 230px;
		border-radius: 5px !important;
	}

	#initial_video_placeholder i{
		position: absolute;
		top: 50%;
		right: 50%;
		transform: translate(50%, -50%);
		color: #5daaa7;
		font-size: 4em;
	}

	#right_images_controls{
		position: absolute;
		right: 10px;
		z-index: 2;
		display: inline-table;
	}

	#form_functions{
		position: fixed;
		right: 30px;
		bottom: 70px;
		z-index: 2;
	}

	#form_functions > div.img-zoom {
		position: relative;
		display: block;
		border-radius: 15px !important;
		background-color: #5daaa7;
		margin: 10px 0;
		cursor: pointer;
	}

	#form_functions .tutorial-text-box{
		right: 60px;
		top: 0;
	}

	#form_functions i {
		font-size: 1.2em;
		padding: 15px;
		color: white;
	}

	#submit_button i{
		margin-left: 3px;
	}

	#tutorial_button i{
		margin-left: 6px;
	}

	#form_functions > div.img-zoom:hover {
		background-color: #fcc946;
	}

	#form_functions > div.on {
		background-color: #fcc946;
	}

	#bottom-help-img{
		display: none !important;
	}

	#close_error_message{
		color: #ff0000bf;
		text-align: center;
		font-size: 4em;
		margin-top: 30px;
	}

	#info_message{
		color: #5daaa7;
		text-align: center;
		font-size: 4em;
		margin-top: 30px; 
	}

	#confirm_choice{
		margin: 0;
	}

	/** Css Layou Admin*/

	.page-header{
		transition: 0.5s ease all;
	}

	.page-header.preview{
		margin-top: -50px;
	}

	.page-sidebar-wrapper{
		position: fixed;
		width: 235px;
	}

	.page-sidebar{
		width: 100%;
	}

	.page-content-wrapper .page-content{
		overflow: auto;
		padding: 0px !important;
		transition: 0.5s ease all;
	}

	.page-footer{
		display: none;
	}

	.page-content.preview{
		margin-left: 0;
		border: 0;
		margin-top: -20px;
	}

	.page-container.preview{
		margin-top: 0;
		position: absolute;
		left: 0;
		right: 0;
	}

	#new_image i{
		display: none;
	}

	/** Css Layou Admin*/

	.page-header{
		transition: 0.5s ease all;
	}

	.page-header.preview{
		margin-top: -50px;
	}

	.page-sidebar-wrapper{
		position: fixed;
		width: 235px;
	}

	.page-sidebar{
		width: 100%;
	}

	.page-content-wrapper .page-content{
		overflow: auto;
		padding: 0px !important;
		transition: 0.5s ease all;
	}

	.page-footer{
		display: none;
	}

	.page-container.preview{
		margin-top: 0;
		position: absolute;
		left: 0;
		right: 0;
	}

	#new_image i, #new_video i{
		display: none;
	}

	#solid_color_input_wrapper{
		position: relative;
	}

	#solid_color{
		border: none;
	}

	#add_solid_color{
		position: absolute;
		right: 5px;
    	top: 10px;
		color: #58a4a0;
		z-index: 5;
		font-size: 1.5em;
		cursor: pointer;
		transition: 0.5s ease color;
	}

	#add_solid_color:hover{
		color: #fccf5a;
	}

	.input-group.color .input-group-btn i {
		position: absolute;
		display: block;
		cursor: pointer;
		width: 100%;
		height: 100%;
		right: 0;
		top: 0;
	}

	.btn.default{
		width: 100%;
		height: 100%;
	}

	/** Hidding buttons that are not working in the note editor */
	.note-editor .btn-fullscreen, .note-editor .dropdown-toggle, .note-editor .note-current-color-button{
		display: none;
	}

	.hide-content.preview{
		display: none !important;
	}

	/** Accordion style */

	.panel.panel-default{
		border: none;
		border-bottom: 1px solid #0000002b;
	}

	.panel-heading{
		background-color: #0000 !important;
	}

	a.accordion-toggle{
		color: #fcc946;
		transition: color ease 0.5s;
	}

	a.accordion-toggle.collapsed{
		color: black;
	}

	@media (max-width: 1680px){

		.container{
			width: 1200px;
		}

		.select-image-item{
			width: 30%;
		}

		.select-video-item{
			width: 30%;
		}
	}

	@media (max-width: 1500px){

		.container{
			width: 1100px;
		}

		.select-image-item, .select-video-item{
			width: 30%;
		}

		#colorpicker{
			height: 70px;
		}

	}

	@media (max-width: 1300px){

		#images_controls .btn{
			margin: 0;
		}

		.select-image-item, .select-video-item{
			width: 29%;
		}

	}

	@media (max-width: 1100px){

		.container{
			width: 750px;
		}

		.preview .container{
			width: 970px;
		}

		.button-preview-copy{
			margin: 0;
		}

		#app_stores{
			width: 70%;
		}

		.icon-editable.flags {
			right: 0px;
		}

		.select-image-item {
			width: 45%;
		}

		.select-video-item {
			width: 45%;
		}

		#headline_edition_confirm{
			right: 30px;
		}

		#social_warning{
			margin-bottom: 50px;
		}

		#footer_login_wrapper{
			text-align: center;
		}

		#share_text{
			margin-left: 0;
		}

		#footer_card{
			padding: 50px 50px 10px 50px;
		}

		.preview #footer_card{
			padding: 50px 100px 10px 100px;
		}

		#footer_login_wrapper{
			width: 50%;
		}

		.view_form .social_row .you_tube, .view_form .social_row .fb{
			width: 100%;
			min-height: inherit;
		}

		.you_tube{
			padding-right: 35px;
		}

		#footer_login_wrapper .btn{
			margin: 0 10px;
			font-size: 11px;
		}

		#icons{
			width: 50%;
		}

		#icons i{
			font-size: 1.8em;
		}

		#new_image, #new_video{
			border: none;
		}

		#new_image span, #new_video span{
			display: none;
		}

		#new_image i, #new_video i{
			display: inherit;
			font-size: 1.5em;
			padding-top: 5px;
		}
	}

	@media (max-width: 992px){
		
		.container{
			width: 900px;
		}

		.page-content-wrapper .page-content.preview{
			margin-top: -10px !important;
		}

		.select-image-item{
			width: 48%;
		}

		.select-video-item{
			width: 48%;
		}

		.preview #footer_card {
			padding: 20px;
		}
	}

	@media (max-width: 768px){

		.container{
			width: 700px;
		}

		.preview .container{
			width: 700px;
		}

		.preview #footer_card {
			padding: 20px;
		}

		#flag_wrapper{
			top: 15px;
			transform: translateX(-50%);
		}

		#header_logo_wrapper{
			margin-top: 50px;
		}

		#header_logo_edition{
			top: 60px;
		}

		#convite_text_wrapper{
			display: inline-block;
		}

		#invitation_headline_wrapper{
			position: relative;
			right: inherit;
			top: inherit;
			transform: none;  
			margin-top: 30px;
		}

		.view_form .social_row .you_tube, .view_form .social_row .fb {
			width: 50%;
			min-height: 100px;
		}

		#invitation_headline p {
			text-align: center;
			margin-left: 0px;
		}

		#social_warning{
			margin-bottom: 50px;
		}

		#footer_login_wrapper{
			text-align: center;
			width: 100%;
		}

		#share_text{
			margin-left: 0;
		}

		iframe{
			margin-bottom: 20px;
		}

		#icons{
			width: 100%;
			text-align: center;
			margin-top: 25px;
		}

		#new_image, #new_video{
			padding: 10px;
		}

		#crop_image{
			display: none;
		}

		#app_stores{
			width: 80%;
		}

		.select-image-item{
			width: 100%;
			height: 110px;
		}

		.select-video-item{
			width: 100%;
		}

		.tutorial-text-box {
			top: 25px;
		}
	}

	@media (max-width: 480px){
		
		.btn-multiplier{
			margin: 5px 0;
		}

		.container{
			width: 320px;
		}

		.preview .container{
			width: 320px;
		}

		.preview #footer_card {
			padding: 20px;
		}

		#images_preview{
			width: 100%;
		}

		#image_show_wrapper{
			display: none;
		}

		#images_preview .panel-collapse .panel-body{
			max-height: 180px; 
		}

		#flag_wrapper{
			top: 15px;
			transform: none;
		}

		#header_logo_wrapper{
			margin-top: 100px;
		}

		#convite_text_wrapper{
			padding: 20px;
		}

		#invitation_headline_wrapper{
			position: relative;
			right: none;
			top: none;
			transform: none;  
		}

		#invitation_headline p {
			text-align: center;
			margin-left: 0px;
		}

		.view_form .social_row{
			padding: 25px;
		}

		.choose_one{
			padding: 0;
		}

		#col_social_text{
			padding: 0;
		}

		#col_social_form{
			padding: 0;
		}

		.view_form .social_row .you_tube, .view_form .social_row .fb {
			width: 50%;
			min-height: inherit;
		}

		.you_tube{
			padding: 10px 0px 0 40px;
		}

		.fb-login{
			padding: 0;
			padding-left: 35px;
		}

		#social_warning{
			font-size: 0.95em;
			padding: 20px;
			line-height: 1.5;
			margin-bottom: 15px;
		}

		#social_warning p {
			margin: 0;
		}

		#social_image {
			margin-left: -20px;
		    max-width: 220px;
		}

		.form-group .row > div{
			margin-bottom: 15px;
		}

		.row.logo_row{
			padding: 0;
		}

		.logo_row hr{
			margin: 20px 0;
		}

		#share_text{
			float: none;
		}

		.button-preview-copy{
			float: none;
		}

		#footer_card{
			padding: 20px;
		}

		#footer_logo{
			width: 90%;
		}

		#footer_login_wrapper .btn {
			margin: 0px 5px;
			font-size: 13px;
		}

		.footer-tag-img {
			width: 45%;
		}

		iframe{
			height: 200px !important;
		}

		.knowMore_row{
			margin: 0px 0 40px 0; 
		}

		#app_stores{
			width: 100%;
		}
		
		#icons{
			padding: 0;
		}

		.delete-image-icon {
			right: unset;
    		left: 5px;
		}
	}

	@media (max-height: 800px){
		#images_preview .panel-collapse .panel-body{
			max-height: 200px; 
		}
	}

</style>

<?php

	$form_data = json_decode( $FormListingData['form_body']);
	$form_data = json_decode( $form_data, true);
	$session = $this->request->session();
	$themeFa =  '<i class="fa fa-gift"></i>';
	if($session->read( 'social.is_login' ) == "google"){
	$themeFa = '<i class="fa fa-youtube"></i>';
	$theme = $session->read('social.theme');
		}
	$is_success_wizard=false;

	$staticimagepath = S3_MEDIA_PATH_ELECTRONICS_STATIC_IMAGES;

	$staticvideopath = S3_MEDIA_PATH_ELECTRONICS_STATIC_VIDEO;
	$dynamicvideopath = S3_MEDIA_PATH_ELECTRONICS_USER_VIDEO;
	$formingpath = S3_MEDIA_PATH_ELECTRONICS_USER_IMAGES;

	// echo "<pre>";
	// var_dump($formInfo);
	// exit();

?>

<link rel = "stylesheet" href = "https://fonts.googleapis.com/css?family=Roboto&display=swap" />

<link rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" />

<form role="form" class="form-horizontal" method="POST" id="edition_form">

	<input class = "hide" value="<?php if(!empty($formId)){echo $formId;} ?>" name="form_id" id="form_id">
	<input class = "hide" id = "background_edition_input" type = "file" accept = "image/*">
	<input class = "hide" id = "chosen_background_image_base_64" name = "chosen_background_image_base_64">

	<input class = "hide" id = "no_video" name = "no_video"> 
	<input class = "hide" id = "chosen_video" name = "chosen_video">
	<input class = "hide" id = "chosen_video_id" name = "chosen_video_id">

	<input class = "hide" id = "chosen_image" name = "chosen_image">

	<div class="page-wrapper">

		<!-- BEGIN HEADER & CONTENT DIVIDER -->
		<div class="clearfix"> </div>
		<!-- END HEADER & CONTENT DIVIDER -->
		<!-- BEGIN CONTAINER -->
		<div class="page-container_dummy editable-content tutorial-step" data-tutorial-show-parent = false data-tutorial-name = " <?= __(backgroundImageTitle) ?>" data-tutorial-text = "<?= __(defaultEditionText) ?>">
			
			<i class="fas fa-edit icon-editable icon-white tooltip-tippy" id = "background_edition" data-tippy-content = "<?= __(backgroundTooltip) ?>"></i>

			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<!-- BEGIN CONTENT BODY -->
				<div class="page-content view_form_page">

					<div class="container">

						<div class="row view_form">

							<div class="col-sm-12">

								<?php 
									$selectedLanguage='Portuguese';
									$gifImage='';
									if($session->read('language') == "en_US"){
										$selectedLanguage='English';
										$url = "../img/facebook_login.png";
										$yurl = "../img/you_tube.png";
										$gifImage = "../img/public_en.gif";
									}else if($session->read('language') == "en_SP"){
										$selectedLanguage='Spanish';
										$url = "../img/facebook_login.png";
										$yurl = "../img/you_tube.png";
										$gifImage = "../img/public_es.gif";
									}else{
										$selectedLanguage='Portuguese';
										$url = "../img/facebook_login.png";
										$yurl = "../img/you_tube.png";
										$gifImage = "../img/public_pt.gif";
									}     

									$text = ($form_description_lang ) ? $form_description_lang : __(causeDescritionText);
									$headline = ($form_title_lang ) ? $form_title_lang : __(causeTitle);
								?>

								<canvas width = 500 height = 500 class = "hide" id = "canvas"></canvas>
					
								<div class = "row" id = "header_row">

									<div class = "col-md-3" id = "header_logo_wrapper">
										<img class = "img-zoom editable-content tutorial-step" data-tutorial-name = "<?= __(Logo) ?>" data-tutorial-text = "<?= __(defaultEditionText) ?>" id = "header_logo" src = "<?= $formInfo->selected_header_image; ?>">
										<i class="fas fa-edit icon-editable icon-white tooltip-tippy" id = "header_logo_edition" data-tippy-content = "<?= __(logoTooltip) ?>" ></i>

										<input class = "hide edition-input" id = "header_logo_edition_input" type = "file" accept = "image/*">
										<input class = "hide" id = "chosen_header_logo_image_base_64" name = "chosen_header_logo_image_base_64">
									</div>

									<div class = "col-md-9"  id = "flag_wrapper">

										<div class = "row flag_row tutorial-step" data-tutorial-name = "<?= __(languageTitle) ?>" data-tutorial-text = "<?= __(languageText) ?>" >

											<div class = "editable-content"> 
												
												<a href="<?php echo HTTP_ROOT ;?>app/changeLanguage?language=2" class="flag <?php echo ($session->read('language') == "en_BR")?'active':''; ?>" >
													<img class = "img-zoom" src="<?php echo '../img/brasilflag.jpg' ; ?>" / >
												</a> 

												<a href="<?php echo HTTP_ROOT ;?>app/changeLanguage?language=1" class="flag <?php echo ($session->read('language') == "en_US")?'active':''; ?>">
													<img class = "img-zoom" src="<?php echo '../img/usaflag.jpg' ; ?>" / >
												</a>

												<a href="<?php echo HTTP_ROOT ;?>app/changeLanguage?language=3" class="flag <?php echo ($session->read('language') == "en_SP")?'active':''; ?>">
													<img class = "img-zoom" src="<?php echo '../img/spanish.jpg' ; ?>" / > 
												</a> 

												<input class = "hide edition-input" id = "language_edition_input" name = "language" value = "<?= $selectedLanguage ?>">

											</div>

											<i class="fas fa-edit icon-editable flags icon-white tooltip-tippy hide next-release" data-tippy-content = "<?= __(languageTooltip) ?>"></i>

										</div>
							
									</div>

								</div>
					
								<div class="row detail_text">

									<div class = "col-md-5" id = "invitation_headline_wrapper">
										<h2 class = "editable-content tutorial-step" 
											data-tutorial-name = "<?= __(causeTitle) ?>" 
											data-tutorial-text = "<?= __(defaultEditionText) ?>"
											id = "invitation_headline"><?= $headline ?> </h2>
										<i class="fas fa-edit icon-editable icon-white tooltip-tippy" id = "headline_edition_button" data-tippy-content = "<?= __(causeNameTooltip) ?>"></i>
										<i class="far fa-check-circle hide img-zoom tooltip-tippy" id = "headline_edition_confirm" data-tippy-content = "<?= __(confirmEdition) ?>"></i>
										<textarea class = "hide edition-input" id = "headline_edition_input" type = "text" name = "headline"></textarea>
									</div>

									<div class = "col-md-7" id = "convite_text_wrapper" >
										<div>

											<div class = "editable-content" id = "main_text">
												<?= $text ?>
											</div>

											<i class="fas fa-edit icon-editable icon-black tooltip-tippy" id = "main_text_edition_button" data-tippy-content = "<?= __(causeDescritionTooltip) ?>"></i>
											<i class="far fa-check-circle hide img-zoom tooltip-tippy" id = "main_text_edition_confirm" data-tippy-content = "<?= __(confirmEdition) ?>"></i>

											<div class="col-sm-12 hide edition-input tutorial-step" id = "main_text_edition"
												data-tutorial-name = "<?= __(causeDescritionTitle) ?>"
												data-tutorial-text = "<?= __(causeDescritionText) ?>"
											> 
												<?php 
													echo $this->Form->input('main_text', [
														'label'=>false,
														'class'=>'form-control col-md-7 col-xs-12 summernote',
														'style'=>'width: 100%; min-height: 240px',
														'id'	=> 'main_text',
														'type' => 'textarea',
														'value' => $form_description_lang
													]);
												?>
											</div>

										</div>

										<div id = "initial-video"> 
											<i class="fas fa-edit icon-editable icon-white tooltip-tippy" id = "main_video" data-tippy-content = "<?= __(addVideoTooltip) ?>"></i>
											<i class="fas fa-trash-alt icon-editable icon-white tooltip-tippy img-zoom <?= ($formInfo->selected_video) ? "" : "hidden" ?>" id = "remove_video" data-tippy-content = "<?= __(removeVideoTooltip) ?>"></i>
											
											<div class = "hide-content <?= ($formInfo->selected_video) ? "hide" : "" ?>" id = "initial_video_placeholder">
												<i class="far fa-play-circle"></i>
											</div>

											<video class = "editable-content <?= ($formInfo->selected_video) ? "" : "hide" ?>"  width="100%" height="100%" controls >
												<source src="<?= $formInfo->selected_video ?>" type="video/mp4">
											</video>
										</div>
									</div>
								
								</div>


								<div class="row social_row ">

								<div class = "col-md-6" id = "col_social_text">

									<div class = "col-md-12">
										<img class = "img-zoom" id = "social_image" src = "../img/logo_big.jpg">
									</div>

									<div class="col-sm-12 choose_one">
										<p id = "privacy_text" > <?= __(defaultPrivacyText) ?> <br> <?= __(formRegistrationPrivacy);?> </p>
									</div>

								</div>

								<div class = "col-md-6" id = "col_social_form"> 

									<div class="col-sm-12 col-form-comming-users">

										<form method="post" id="formsubmit" action="" class="formCommingUsers">

											<div class="form-group">
												<label class="control-label"><?php echo __(formRegistrationName); ?>*</label>
												<?php echo $this->Form->input('name', ['label' => false,'class'=>'form-control']); ?>
												<span id="form_error1" class="help-block help-block-error"></span>
											</div>

											<div class="form-group">
												<label class="control-label"><?php echo __(formRegistrationEmail); ?>*</label>
												<?php echo $this->Form->input('email', ['label' => false,'class'=>'form-control', 'type'=>'email']); ?>
												<span id="form_error1" class="help-block help-block-error"></span>
											</div>

											<div class="form-group">

												<label class="control-label"><?php echo __(formRegistrationMobile); ?>*</label>
												<div class="row">

													<div class="col-sm-3" style="clear: both;">
														<img id="countryflag" src="../img/flags/br.png" width="50"/>
													</div>

													<div class="col-sm-9">
														<select id="countries_dropdown" class="form-control" onChange="countyChanged()"> 

															<?php foreach($countries as $country){ 
															if(file_exists($_SERVER["DOCUMENT_ROOT"].'/app/webroot/img/flags/'.strtolower($country->iso2).'.png')){ ?>
																<option value="<?php echo strtolower($country->iso2) ?>" data-contry-id="<?php echo $country->id; ?>" data-phone="<?php echo $country->phonecode; ?>" data-flag="<?php echo '/app/webroot/img/flags/'.strtolower($country->iso2).'.png';  ?>"><?php echo $country->name; ?></option>
															<?php }}?>

														</select>
													</div>

													<span id="form_error71" class="help-block help-block-error"></span>

												</div>

												<br/>

												<div class="row">

													<div class="col-sm-3" style="clear: both;">
													<input type="text" id="countrycode" class="form-control" value="+55" readonly />
													</div>

													<div class="col-sm-9">
													<?php echo $this->Form->input('phone', ['label' => false,'class'=>'form-control']); ?>
													</div>

													<span id="form_error7" class="help-block help-block-error"></span>
												
												</div>

											</div>

										</form>
									</div>

									<div class="col-sm-6 fb fb-login"> <a id="get_values_facebook" href="javascript:void(0);" ><img src="<?php echo $url; ?>"></a> </div>
									<div class="col-sm-6 you_tube"> <a  href="javascript:void(0);" id="get_values_gmail"> <img src="<?php echo $yurl;?>"></a> </div> 

								</div>

								<div class="row fb_public fb_login_box" style="display:none">
									<div class="col-sm-12 fb_border">

										<div class="col-sm-12">
											<p class="heading"><?php echo __(social_select2);?></p>
										</div>

										<div class="col-sm-12">
											<img src="<?php echo $gifImage; ?>" >
										</div>

										<div class="col-sm-12">
											<button type="button" class="btn back_btn" style="float:left;margin-left:10px;"  onclick="$('.fb_login_box').hide();$('#col_social_text, #col_social_form, .flag_row, .img_row').show();">  <i class=" fa fa-angle-left"></i> <?php echo __(back);?>  </button>
											<button type="button" class="btn next_btn"  id="get_values"><?php echo __(next);?>  <i class=" fa fa-angle-right"></i></button>
										</div>

									</div>
								</div>

								<div class="row logo_row tutorial-step" data-tutorial-name = "<?= __(sharingTitle) ?>" data-tutorial-text = "<?= __(sharingText) ?>">

								<div class="col-sm-12"> 

									<hr>
									<h5 id = "share_text"> <b> <?php echo __(ShareThisInvitationToYourFriends); ?> </b> </h5>
									<div class="button-preview-copy ">
										<div class="sharethis-inline-share-buttons" 
											data-title="<?php echo __(sharethis_title);?>" 
											data-url="<?php  echo HTTP_ROOT; ?>commingusers/view-form?id=<?php echo $FormListingData['form_id']; ?>" 
											data-description="<?php echo __(sharethis_description);?>" data-image="<?php echo HTTP_ROOT.'img/uploads/'.(@$siteinfo->site_logo != ''?@$siteinfo->site_logo:'logo.png'); ?>">
										</div>
									</div>

								</div>

							</div>

						</div>

						<div id = "footer_card">

							<div class = "row" id = "footer_wrapper">

								<div class = "col-md-6">
								<p><a href="https://multiplierapp.com.br/"> <img class = "img-zoom" id = "footer_logo" src="../img/logo_big.jpg"> </a> </p>
								</div>

								<div class="col-md-5" id = "footer_login_wrapper">
									<a id="create_account_link" class = "btn btn-multiplier" href = "https://multiplierapp.com.br/politicas-de-uso/"> Criar Conta </a>
									<a id="login_link" class = "btn btn-multiplier btn-green" href = "https://multiplierapp.com.br/app/users/login"> Conecte-se </a>
								</div>

							</div>

							<div class="row advertising_row">

								<div class="col-md-6"> 
									<iframe width="100%" height="315" src="https://www.youtube.com/embed/SepR-FOG51w" frameborder="0" allowfullscreen></iframe>
								</div>

								<div class = "col-md-6">
									<iframe width="100%" height="315" id="youtube_video" src="https://www.youtube.com/embed/Z4j3bwCHKqo?feature=oembed&amp;start&amp;end&amp;wmode=opaque&amp;loop=0&amp;controls=1&amp;mute=0&amp;rel=0&amp;modestbranding=0"> </iframe>
								</div>

							</div>

							<div class="row knowMore_row">

								<div class="col-md-6">
									<p> <?php echo __(KnowMoreAboutUsAt); ?> <a href="http://www.multiplierapp.com.br/" target="_blank">www.multiplierapp.com.br</a></p>
								</div>

								<div class = "col-md-5" id = "icons">

									<a target="_blank" href = "https://www.instagram.com/multiplierapp/"> <i class="fab fa-instagram img-zoom"></i> </a>
									<a target="_blank" href = "https://www.facebook.com/multiplierapp/"> <i class="fab fa-facebook-f img-zoom"></i> </a>
									<a target="_blank" href = "https://www.youtube.com/c/MultiplierApp"> <i class="fab fa-youtube img-zoom"></i> </a>
									<a target="_blank" href = "https://twitter.com/MultiplierApp"> <i class="fab fa-twitter img-zoom"></i> </a>
									<a target="_blank" href = "https://www.linkedin.com/company/multiplierapp/"> <i class="fab fa-linkedin-in img-zoom"></i> </a>

								</div>

							</div>

							<div class = "row">
								
								<div class = "col-md-12" id = "app_stores_wrapper">

									<div id = "app_stores">
										<a target="_blank" href = "https://play.google.com/store/apps/details?id=com.br.multiplierapp"> <img class = "img-zoom footer-tag-img" src = "http://www.multiplierapp.fcdesigner.art.br/wp-content/uploads/2019/11/Multiplierapp-Baixe-no-google-play.png"> </a>
										<a target="_blank" href = "https://apps.apple.com/app/multiplierapp/id1301426934?ls=1"> <img class = "img-zoom footer-tag-img" src = "http://www.multiplierapp.fcdesigner.art.br/wp-content/uploads/2019/11/Multiplierapp-Baixe-no-apple-store.png"> </a>
									</div>
								</div>

							</div>

							<div class="row">

								<div class = "col-md-12">
									<p id = "text_rights">@2020 MultiplierApp – Todos os direitos reservados / All rights reserved / Todos los derechos reservados.</p>
								</div>

							</div>
						</div>

					</div>

				</div>
			</div>

		</div>
		<!-- END CONTENT BODY -->
	</div>
	
	<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->
	</div>

	<div class = "tutorial-overlay">
	</div>

	<div class = "hide" id = "images_modal">

		<div class = "row" id = "images_wrapper">

			<i class="fas fa-times img-zoom" id = "close_modal_images"></i>

			<div class = "col-sm-4" id = "images_preview">

				<h4 id = "gallery_name"> <?= __(gallery) ?> </h4>
				<hr>

				<div class="portlet-body">
					<div class="panel-group accordion" id="accordion">

						<!-- Default Images -->
						<div class="panel panel-default panel-image">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle accordion-toggle-styled collapsed" id = "avaliable_images_accordion" data-toggle="collapse" data-parent="#accordion" href="#avaliable_images" aria-expanded="false"> <?= __(avaliableImages) ?> </a>
								</h4>
							</div>
							<div id="avaliable_images" class="panel-collapse collapse" aria-expanded="false">
								<div class="panel-body">
		
									<?php foreach($elecStaticImageData as $avaliable_image) { ?>
										<div class = "select-image-item background">
											<img class = "img-zoom" data-id = "<?= $avaliable_image->id ?>" src = "<?= $staticimagepath.$avaliable_image->img_name ?>">
										</div>
									<?php } ?>

								</div>
							</div>
						</div>

						<!-- Default Videos -->
						<div class="panel panel-default panel-video">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle accordion-toggle-styled collapsed" id = "avaliable_videos_accordion" data-toggle="collapse" data-parent="#accordion" href="#avaliable_videos" aria-expanded="false"> <?= __(avaliableVideos) ?></a>
								</h4>
							</div>
							<div id="avaliable_videos" class="panel-collapse collapse" aria-expanded="false">
								<div class="panel-body">
		
									<?php foreach($elecStaticVideoData as $avaliable_video) { ?>
										<div class = "select-image-item videos">
											<video class = "img-zoom">
												<source src="<?= $staticvideopath.$avaliable_video->video_name ?>" data-id="<?= $avaliable_video->id ?>" data-val="<?= $staticvideopath.$avaliable_video->video_name ?>" type="video/mp4">
											</video>
										</div>
									<?php } ?>
									
								</div>
							</div>
						</div>

						<!-- User Images -->
						<div class="panel panel-default panel-image">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle accordion-toggle-styled collapsed" id = "my_images_accordion" data-toggle="collapse" data-parent="#accordion" href="#my_images" aria-expanded="false"> <?= __(myImages) ?></a>
								</h4>
							</div>
							<div id="my_images" class="panel-collapse collapse" aria-expanded="false">
								<div class="panel-body">
								
									<div class = "select-image-item add-file-wrapper">
										<div>
											<i class="fas fa-plus-circle add-file" id = "add_image"></i>
										</div>
									</div>

									<?php foreach($elecDataImage as $user_image) { ?>
										<div class = "select-image-item background">
											<i class="fas fa-trash-alt img-zoom delete-image-icon"></i>
											<img class = "img-zoom" data-id = "<?= $user_image->id ?>" src = "<?= $formingpath.$user_image->videoName ?>">
										</div>
									<?php } ?>

								</div>
							</div>
						</div>

						<!-- User Logos -->
						<div class="panel panel-default panel-image">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle accordion-toggle-styled collapsed" id = "my_logos_accordion" data-toggle="collapse" data-parent="#accordion" href="#my_logos" aria-expanded="false"> <?= __(myLogos) ?></a>
								</h4>
							</div>
							<div id="my_logos" class="panel-collapse collapse" aria-expanded="false">
								<div class="panel-body">
								
									<div class = "select-image-item add-file-wrapper">
										<div>
											<i class="fas fa-plus-circle add-file" id = "add_logo"></i>
										</div>
									</div>

									<?php foreach($elecDataLogo as $user_image) { ?>
										<div class = "select-image-item logos">
											<i class="fas fa-trash-alt img-zoom delete-image-icon"></i>
											<img class = "img-zoom" data-id = "<?= $user_image->id ?>" src = "<?= $formingpath.$user_image->videoName ?>">
										</div>
									<?php } ?>

								</div>
							</div>
						</div>

						<!-- User Videos -->
						<div class="panel panel-default panel-video">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle accordion-toggle-styled collapsed" id = "my_videos_accordion" data-toggle="collapse" data-parent="#accordion" href="#my_videos" aria-expanded="false"> <?= __(myVideos) ?></a>
								</h4>
							</div>
							<div id="my_videos" class="panel-collapse collapse" aria-expanded="false">
								<div class="panel-body">
								
									<div class = "select-image-item add-file-wrapper">
										<div>
											<i class="fas fa-plus-circle add-file" id = "add_video"></i>
										</div>
									</div>

									<?php foreach($elecData as $user_video) { ?>
										<div class = "select-image-item videos">
											<i class="fas fa-trash-alt img-zoom delete-image-icon"></i>
											<video class = "img-zoom">
												<source src="<?= $dynamicvideopath.$user_video->videoName_thumb ?>" data-id="<?= $user_video->id ?>" data-val="<?= $dynamicvideopath.$user_video->videoName_360 ?>" type="video/mp4">
											</video>
										</div>
									<?php } ?>

								</div>
							</div>
						</div>										

						<!-- Solid Color  -->
						<div class="panel panel-default panel-image hide">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle accordion-toggle-styled collapsed" id = "colors_accordion" data-toggle="collapse" data-parent="#accordion" href="#colors" aria-expanded="false"> <?= __(colors) ?> </a>
								</h4>
							</div>
							<div id="colors" class="panel-collapse collapse" aria-expanded="false">
								<div class="panel-body">

									<div class = "select-image-item" id = "select_colorpicker">

										<div class="input-group color colorpicker-default" id = "colorpicker" data-color="#000000">                							 
											<span class="input-group-btn">
												<button class="btn default" type="button">
													<i style="background-color: rgb(114, 29, 166);"></i>&nbsp;
												</button>
											</span>
										</div>

										<div class="input text" id = "solid_color_input_wrapper">
											<i class="fas fa-plus" id = "add_solid_color"></i>
											<input type="text" name="theme_color" class="form-control col-md-7 col-xs-12" id="solid_color" value="#000000">
										</div>    

									</div>

								</div>
							</div>
						</div>

					</div>
				</div>

			</div>

			<div class = "col-sm-8" id = "image_show_wrapper">

				<h4 id = "chosen_image"> <?= __(chosenImage) ?> </h4>

				<hr>

				<div id = "image_holder_wrapper">

					<img id = "image_holder" name = "" src ="<?php echo $staticimagepath."image1.jpg"; ?>">

					<img id = "logo_holder" name = "" src = "">

					<video class = "hide" id = "preview_video_holder" controls>
						<source id = "video_holder" src="<?php echo $staticvideopath."video1.mp4"; ?>" type="video/mp4">
					</video>

				</div>

			</div>

		</div>

		<div id = "images_controls">

			<form enctype="multipart/form-data" method="post">
					
				<input class = "hide" value="<?php if(!empty($formId)){echo $formId;} ?>" name="frm_id" id="frm_id">
						
				<?= $this->Form->input('addvideogallary',[
						'type' => 'file',
						'label' =>  false,
						'name' =>'addvideogallary',
						'class'=>'addvideogallary_video_input btn btn-multiplier btn-green hide']);
				?>
					
			</form>

			<a class = "btn btn-multiplier btn-green" id = "confirm_choice"> 
				<span id = "confirm_choice_text"><?= __(confirmImageChoice) ?></span> 
				<span class = "hide" id = "uploading_file_text"> Uploading </span>
				<div class="lds-ellipsis hide"><div></div><div></div><div></div><div></div></div>
			</a>

			<div id = "right_images_controls">
				<a class = "btn btn-multiplier vanilla-rotate hide" data-deg="-90"><i class="fa fa-rotate-right"></i></a>
				<a class = "btn btn-multiplier vanilla-rotate hide" data-deg="90"><i class="fa fa-rotate-left"></i></a>
				<!-- <a class = "btn btn-multiplier" id = "crop_image"> <?= __(resizeImage) ?> </a> -->
				<a class = "btn btn-multiplier btn-green hide" id = "crop_image_confirm"> <?= __(save) ?> </a>
				<a class = "btn btn-multiplier hide" id = "crop_image_cancel"> <?= __(cancel) ?> </a>
			</div>

		</div>
		
	</div>

	<div id = "form_functions">
		
		<div class = "img-zoom tooltip-tippy tutorial-step" 
			data-tutorial-name = "<?= __(resourcesTitle) ?>" 
			data-tutorial-text = "<?= __(resourcesText) ?>"
			id = "preview_button" data-tippy-content = "<?= __(previewInvitation) ?>"
		>
			<i class = "far fa-eye"></i>
		</div>

		<div class = "img-zoom tooltip-tippy" id = "gallery_button" data-tippy-content = "<?= __(openGallery) ?>">
			<i class = "far fa-images"></i>
		</div>

		<?php if($formId) { ?>

			<div class = "img-zoom tooltip-tippy" id = "copy_link_button" data-tippy-content = "<?= __(copyInvitationLink) ?>">
				<i class="fas fa-link" ></i>
				<span class = "hide" id = "invitation_link"><?php echo HTTP_ROOT; ?>commingusers/view-form?id=<?php echo $formId; ?></span>
			</div>

		<?php } ?>

		<div class = "img-zoom tooltip-tippy" id = "tutorial_button" data-tippy-content = "<?= __(showTutorialAgain) ?>">
			<i class = "fas fa-info"></i>
		</div>

		<div class = "img-zoom tooltip-tippy" id = "submit_button" data-tippy-content = "<?= __(saveInvite) ?>">
			<i class = "fas fa-save"></i>
		</div>
		
	</div>

</form>

<a class = "hide" target="_blank" href="https://multiplierapp.atlassian.net/servicedesk/customer/portal/1"><img class = "img-zoom hide" src="<?php echo HTTP_ROOT ;?>img/1962073065-Help.png" id="bottom-help-img"></a> 

<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>                 
<script src="../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="//platform-api.sharethis.com/js/sharethis.js#property=5b362317c5ed9600115214d3&product=inline-share-buttons"></script>
<script type="text/javascript"> var base_url = '<?php echo HTTP_ROOT; ?>';</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/inputmask/4.0.9/inputmask/inputmask.min.js" integrity="sha256-OeQqhQmzxOCcKP931DUn3SSrXy2hldqf21L920TQ+SM=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/inputmask/4.0.9/inputmask/inputmask.extensions.min.js" integrity="sha256-vpL5m6IilsG6TXFKZ99NU+cBmT122RJ6sqBgq0a9/vQ=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/inputmask/4.0.9/inputmask/jquery.inputmask.min.js" integrity="sha256-x4dnYQ/ZWD3+ubQ1kR3oscEWXKZj/gkbZIhGB//kmmg=" crossorigin="anonymous"></script>
<script src="<?php echo HTTP_ROOT; ?>assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js" type="text/javascript"></script>

<link href="<?php echo HTTP_ROOT;?>assets/global/plugins/croppie/croppie.css" rel="stylesheet" type="text/css" />
<script src="<?php echo HTTP_ROOT;?>assets/global/plugins/croppie/croppie.js" type="text/javascript"></script>

<script src="https://unpkg.com/popper.js@1"></script>
<script src="https://unpkg.com/tippy.js@5"></script>

<script type = "text/javascript">

	var updatedImage;
	var formId = "<?= ($formId) ? $formId : "" ?>";
	var language = "<?= $selectedLanguage ?>";
	var change = false;
	var croped_result;

	$( document ).ready(function() {
		
		$(".colorpicker-default").colorpicker({format:"hex"}).on('changeColor', function(ev){
			$("#solid_color").val(ev.color.toHex());
		})
		.on('show', function(){
			var left = parseInt($(".colorpicker").css("left").slice(0, -2)) + $(".select-image-item:visible").width() - 20;
			$(".colorpicker").css("left", `${left}px`);
		});

		$("#add_solid_color").click(function(){
			addBase64File( createImageData64FromColor( $("#solid_color").val() ) );
		});

		$("#solid_color").change(function(){
			$(".colorpicker-default").colorpicker('setValue', $(this).val() );
		});

		$(".icon-editable").click(function(){
			change = true;
		});

		$("a").click(function(e){
			
			if( $(this).attr("href") !== undefined ){

				if( $(this).attr("href").indexOf("http") >= 0 && change){
					
					e.preventDefault();

					$(".container").append(`
						<div class = "tutorial-start-text-box" style = "width: auto;"> 
							<i class="fas fa-info img-zoom" id = "info_message"></i>
							<h3> <?= __(attention) ?> </h3>
							<p> <?= __(saveBeforeExit) ?> </p> 
							<div class = "tutorial-button-wraper">
								<a class = "btn btn-multiplier btn-green" href = "#" onclick = "$('#submit_button').click();"> <?= __(save) ?> </a>
								<a class = "btn btn-multiplier" onclick = "clear_tutorial();" > <?= __(keepEditing) ?> </a>
							</div>
						</div>`
					);

				}

			}
				
		});

		tippy(".tooltip-tippy");

		//Background Image Edition

		$(".select-image-item.background").click(function(){

			$("#preview_video_holder, #logo_holder").addClass("hide");
			$("#image_holder, #crop_image").removeClass("hide");
			$("#image_holder").attr("src", $(this).find('img').attr("src") );
			
			$("#chosen_image_base_64").val( $(this).find('img').attr("data-id") );
			$("#chosen_background_image_base_64").val( $(this).find('img').attr("src") );	

		});

		$(".select-image-item.logos").click(function(){

			$("#preview_video_holder, #image_holder, #crop_image").addClass("hide");
			$("#logo_holder").removeClass("hide");
			$("#logo_holder").attr("src", $(this).find('img').attr('src') );
			
			$("#chosen_header_logo_image_base_64").val( $(this).find('img').attr('src') );	
		});

		$(".select-image-item.videos").click(function(){

			$("#image_holder, #crop_image, #logo_holder").addClass("hide");
			$("#video_holder").attr("src", $(this).find("source").attr("src") );
			$("#preview_video_holder").removeClass("hide").get(0).load();
			
			$("#chosen_video").val( $(this).find("source").attr("data-val") );
			$("#chosen_video_id").val( $(this).find("source").attr("data-id") );
		});

		$(".add-file-wrapper").click(function(){

			if( $(this).find("#add_image").length > 0){
				$('#background_edition_input').click();
			}
			else if( $(this).find("#add_video").length > 0){
				$("#addvideogallary").click();
			}
			else if( $(this).find("#add_logo").length > 0 ){
				$('#header_logo_edition_input').click();
			}
			
		});
		
		$('#background_edition_input').on('change', function () { 
			readFileNew(this, 
			function(e){ 
				addImage(e.target.result);
			});
		});

		$(document).on('click', '.delete-image-icon', function(e){

			delete_element = $(this).parent();

			$(".container").append(`
				<div class = "tutorial-start-text-box" style = 'width: auto;'> 
					<i class="fas fa-info img-zoom" id = "info_message"></i>
					<h3> <?= __(attention) ?> </h3>
					<p> <?= __(deletingFile) ?> </p> 
					<div class = "tutorial-button-wraper">
						<a class = "btn btn-multiplier btn-green" href = "#" onclick = "clear_tutorial(false); deleteItemGallery( delete_element ); $('#images_modal').removeClass('blocked'); "> <?= __(delete) ?> </a>
						<a class = "btn btn-multiplier" onclick = "clear_tutorial(false); $('#images_modal').removeClass('blocked');" > <?= __(go_back) ?> </a>
					</div>
				</div>`
			);

			$("#images_modal").addClass("blocked");

		});

		$("#background_edition, #main_video").click(function(event){

			resizeImagesModal();
	
			var id = $(event.target).prop("id");

			// if(id === "background_edition"){
			// 	$("#preview_video_holder").addClass("hide");
			// 	$("#image_holder, #crop_image").removeClass("hide");
			// 	$(".panel-video").addClass("hide");
			// 	$(".panel-image").removeClass("hide");
			// }
			// else if(id === "main_video"){
			// 	$(".panel-image").addClass("hide");
			// 	$(".panel-video").removeClass("hide");
			// 	$("#image_holder, #crop_image").addClass("hide");
			// 	$("#preview_video_holder").removeClass("hide").get(0).load();
			// }

			$("#images_modal").removeClass("hide");
			$(".tutorial-overlay").removeClass("hide");
		});

		$("#close_modal_images").click(function(){
			$("#images_modal").addClass("hide");
			$(".tutorial-overlay").addClass("hide");
		});

		$("#close_modal_images_video").click(function(){
			$("#images_modal_video").addClass("hide");
			$(".tutorial-overlay").addClass("hide");
		});

		$(document).on("click", ".append-logo" , function() {

			$("#preview_video_holder, #image_holder, #crop_image").addClass("hide");
			$("#logo_holder").removeClass("hide");
			$("#logo_holder").attr("src", $(this).attr('src') );
			
			$("#chosen_header_logo_image_base_64").val( $(this).attr('src') );	
		});		

		$(document).on("click", ".append-img" , function() {

			$("#preview_video_holder, #logo_holder").addClass("hide");
			$("#image_holder, #crop_image").removeClass("hide");
			$("#image_holder").attr("src", $(this).attr("src") );
			
			$("#chosen_image_base_64").val( $(this).attr("data-id") );
			$("#chosen_background_image_base_64").val( $(this).attr("src") );
		});

		$(document).on("click", ".append-video" , function() {
			
			$("#image_holder, #crop_image, #logo_holder").addClass("hide");
			$("#video_holder").attr("src", $(this).find("source").attr("src") );
			$("#preview_video_holder").removeClass("hide").get(0).load();
			
			$("#chosen_video").val( $(this).find("source").attr("data-val") );
			$("#chosen_video_id").val( $(this).find("source").attr("data-id") );

		});

		$(".select-video-tag").click(function(){
			var videopath =  $(this).children("source").attr("src");

			var videoid =  $(this).children("source").attr("data-id");
			var video_360 =  $(this).children("source").attr("data-val");
		
			$("#video_holder_wrapper").html(`
			<video  controls>
				<source id = "video_holder" src="`+video_360+`" type="video/mp4">
			</video>
			`)
			
			$("#chosen_video").val(video_360);
			$("#chosen_video_id").val(videoid);
		});

		$("#confirm_choice").click(function(){

			if( $("#image_holder:visible").length > 0  ){

				var selected_val = $("#chosen_background_image_base_64").val();
				var form_id = "<?php echo $formId; ?>";

				var form_data = new FormData();
				form_data.append("selected_val", selected_val);
				form_data.append('form_id',form_id);
				
				$("#close_modal_images").click();

				var url = window.location.origin + "/app/electronicinvitation/saveSelectedImage";
				$.ajax({				
					url:url,
					method:"POST",
					data: form_data,
					contentType: false,
					cache: false,
					dataType: "text",
					processData: false,
					beforeSend:function(){
						// $(".lds-ellipsis").removeClass("hide");
					},
					success:function(data)
					{
						console.log(data);
						if(data){
							$(".page-content .view_form_page").css( "background-image", `url( ${$("#image_holder").attr("src")} )` );
							updatedImage = $("#image_holder").attr("src");
						}
					},

				});

			}
			else if( $("#logo_holder:visible").length > 0 ){
				$("#close_modal_images").click();
				$("#header_logo").attr("src", $("#chosen_header_logo_image_base_64").val());
			}
			else if( $("#preview_video_holder:visible").length > 0 ){

				var selected_val = $("#chosen_video").val();
				var selected_val_id = $("#chosen_video_id").val();
				var form_id = "<?php echo $formId; ?>";
				//alert(selected_val_id);
				var form_data = new FormData();
				form_data.append("selected_val", selected_val);
				form_data.append("selected_val_id", selected_val_id);
				form_data.append('form_id',form_id);
				
				$("#close_modal_images").click();
				$("#initial-video").children("video").removeClass("hide");
				$("#remove_video").removeClass("hidden");
				$("#initial_video_placeholder").addClass("hide");
				$("#no_video").val(0);

				var url = window.location.origin + "/app/electronicinvitation/saveSelectedVideo";
				$.ajax({
					
					url:url,
					method:"POST",
					data: form_data,
					contentType: false,
					cache: false,
					dataType: "text",
					processData: false,
					beforeSend:function(){
						//$('#loader-show').addClass("loader");
					},
					success:function(data)
					{
						console.log(data);
						//var path = "<?php echo $dynamicvideopath; ?>"+data;
						if(data){
							$("#initial-video video source").attr("src", data );
							$("#initial-video video").get(0).load();
						}
					}
				});

			}

		});

		$("#remove_video").click(function(){

			$(".container").append(`
				<div class = "tutorial-start-text-box" style = 'width: auto;'> 
					<i class="fas fa-info img-zoom" id = "info_message"></i>
					<h3> <?= __(attention) ?> </h3>
					<p> <?= __(removeVideoWarning) ?> </p> 
					<div class = "tutorial-button-wraper">
						<a class = "btn btn-multiplier btn-green" href = "#" onclick = " clear_tutorial(false); removeVideo()"> <?= __(remove) ?> </a>
						<a class = "btn btn-multiplier" onclick = "clear_tutorial(false);" > <?= __(go_back) ?> </a>
					</div>
				</div>`
			);

		});

		//Video
		$("#background_edition_video").click(function(){

			resizeImagesModalVideo();

			$("#images_modal_video").removeClass("hide");
			$(".tutorial-overlay").removeClass("hide");
		});

		//Logo Edition

		$("#header_logo_edition").click(function(){
			// $('#header_logo_edition_input').click();

			resizeImagesModal();

			$("#images_modal").removeClass("hide");
			$(".tutorial-overlay").removeClass("hide");

		});

		$('#header_logo_edition_input').on('change', function () {
			readFile(this, 
			function(e){
				console.log(e);
				// $("#header_logo").attr("src", e.target.result);
				// $("#chosen_header_logo_image_base_64").val(e.target.result);
			}); 
		});

    	//Text Edition

		$("#main_text_edition_button").click(function(){
			
			$("#main_text_edition").removeClass("hide");
			$("#main_text_edition_confirm").removeClass("hide");
			
			$("#main_text").addClass("hide");
			$("#main_text_edition_button").addClass("hide");
		});

		$("#main_text_edition_confirm").click(function(){

			$("#main_text_edition").addClass("hide");
			$("#main_text_edition_confirm").addClass("hide");

			$("#main_text").removeClass("hide");
			$("#main_text_edition_button").removeClass("hide");

			$("#main_text").html( $(".note-editable").html() );

		});

		//Headline Edition

		$("#headline_edition_button").click(function(){

			$("#headline_edition_input").removeClass("hide").val( $("#invitation_headline").text() );
			$("#headline_edition_confirm").removeClass("hide");

			$("#invitation_headline").addClass("hide");
			$("#headline_edition_button").addClass("hide");
		});

		$("#headline_edition_confirm").click(function(){

			$("#headline_edition_input").addClass("hide");
			$("#headline_edition_confirm").addClass("hide");

			$("#invitation_headline").removeClass("hide").text( $("#headline_edition_input").val() );
			$("#headline_edition_button").removeClass("hide");

		});

		// Social Text Edition

		$("#social_title_edition_button").click(function(){

			$("#social_title_edition_input").removeClass("hide").val( $("#social_title").text() );
			$("#social_title_edition_confirm").removeClass("hide");

			$("#social_title").addClass("hide");
			$("#social_title_edition_button").addClass("hide");
		});

		$("#social_title_edition_confirm").click(function(){

			$("#social_title_edition_input").addClass("hide");
			$("#social_title_edition_confirm").addClass("hide");

			$("#social_title").removeClass("hide").text( $("#social_title_edition_input").val() );
			$("#social_title_edition_button").removeClass("hide");

		});

    	//Form Functions

		//Preview
		$("#preview_button").click(function(){

			if( $(".edition-input").not(".hide").length == 0 ){
				$(".page-header").toggleClass("preview");
				$(".page-content").toggleClass("preview");
				$(".page-container").toggleClass("preview");
				$(".icon-editable").not(".next-release").toggleClass("hide");
				$("#preview_button").toggleClass("on");
				$(".hide-content").toggleClass("preview");
			}
			else{

			$(".container").append(`
				<div class = "tutorial-start-text-box"> 
				<i class="fas fa-times img-zoom" id = "close_error_message"></i>
				<h3> Error </h3>
				<p> Por favor termine todas as edições antes de ver o preview da página! </p> 
				<div class = "tutorial-button-wraper">
					<a class = "btn btn-multiplier btn-green" onclick = "clear_tutorial();" > Okay </a>
				</div>
				</div>`
			);

			}

		});

		//Gallery
		$("#gallery_button").click(function(){
			
			resizeImagesModal();

			$("#images_modal").removeClass("hide");
			$(".tutorial-overlay").removeClass("hide");
		});

		//Copy Link
		$("#copy_link_button").click(function(){
			var success = copyToClipboard( $("#invitation_link").get(0) );

			if($success){

			$(".container").append(`
				<div class = "tutorial-start-text-box"> 
				<i class="fas fa-times img-zoom" id = "close_error_message"></i>
				<h3> Link copiado com sucesso </h3>
				<div class = "tutorial-button-wraper">
					<a class = "btn btn-multiplier btn-green" onclick = "clear_tutorial();" > Okay </a>
				</div>
				</div>`
			);

			}

		});

		//Tutorial
		$("#tutorial_button").click(function(){
			if( $(".tutorial-start-text-box").length === 0 ){
			start_tutorial();
			}
		});

		//Save
		$("#submit_button").click(function(){
			$("#edition_form").submit();
		});

		$(".icon-editable").not("#background_edition").hover(
			function(){

				var $parent = $(this).parent();

				if( $parent.hasClass("editable-content") ){
				$parent.addClass("editable-content-highlight");
				}
				else{
				$parent.find(".editable-content").addClass("editable-content-highlight");
				}

			},
			function(){

				var $parent = $(this).parent();

				if( $parent.hasClass("editable-content") ){
				$parent.removeClass("editable-content-highlight");
				}
				else{
				$parent.find(".editable-content").removeClass("editable-content-highlight");
				}
			}
		);

		loadLocalImages();
		loadLocalVideos();
  	});

	function resizeImagesModal(){
		
		var modal_height = $(window).height() - ( $(window).height()/10 ) * 2;

		$("#images_modal").css("height", modal_height);
		$("#images_wrapper").css("height", modal_height - 50);
		$("#images_preview").css("height", modal_height - 50);
		$("#image_holder_wrapper").css("height", modal_height - 100);
	}

	function resizeImagesModalVideo(){
		var modal_height = $(window).height() - ( $(window).height()/10 ) * 2;
		$("#images_modal_video").css("height", modal_height);
		$("#images_wrapper_video").css("height", modal_height - 50);
		$("#images_preview_video").css("height", modal_height - 50);
		$("#video_holder_wrapper").css("height", modal_height - 100);
	}

	function addImage(src, addEvents = true){

		var images = [];

		if(localStorage.getItem("background_images") != null ){
			images = JSON.parse( localStorage.getItem("background_images") );
		}

		// $("#images_preview").append(`
		// 	<div class = "select-image-item">
		// 		<img class = "img-zoom" data-localstorage-id = "${images.length}" src = "${src}">
		// 	</div>
		// `)

		images.push(src)
		localStorage.setItem("background_images", JSON.stringify(images))

		if(addEvents){

			// $(".delete-image-icon").click(function(){
			// 	deleteLocalImage(this);
			// });

			$(".select-image-item img").click(function(){
				//alert();
				$("#image_holder").attr("src", $(this).attr("src") );
				$("#chosen_background_image_base_64").val( $(this).attr("src") );
			});

		}

	}

	function readFileNew(input, callback) {

		if (input.files && input.files[0]) {

			var form_data = new FormData();
			form_data.append("file", input.files[0]);
			form_data.append('form_id',$('#frm_id').val());
			var url = window.location.origin + "/app/electronicinvitation/backgroundImageUpload";
				$.ajax({
				
				url:url,
				method:"POST",
				data: form_data,
				contentType: false,
				cache: false,
				dataType: "text",
				processData: false,
				beforeSend:function(){
					$(".lds-ellipsis, #uploading_file_text").removeClass("hide");
					$("#confirm_choice_text").addClass("hide");
				},
				success:function(data)
				{
					console.log('video data=>',data);
					var formpath = "<?php echo $formingpath; ?>";
					if(data){
						var path = formpath+data;
						$("#my_images .panel-body").append(`
							<div class = "select-image-item background">
								<img class = "img-zoom append-img" data-id = "1" src = "`+path+`">
							</div>
						`)
					}
				
				},
				complete: function(){
					$(".lds-ellipsis, #uploading_file_text").addClass("hide");
					$("#confirm_choice_text").removeClass("hide");
				}
			});
		}
	}

	function addBase64File(base64){

		var form_data = new FormData();
		form_data.append("solid_image_base64", base64);
		form_data.append('form_id', $('#frm_id').val() );
		var url = window.location.origin + "/app/electronicinvitation/solidColorImageUpload";

		$.ajax({
			url:url,
			method:"POST",
			data: form_data,
			contentType: false,
			cache: false,
			dataType: "text",
			processData: false,
			beforeSend:function(){
				//$('#loader-show').addClass("loader");
			},
			success:function(data)
			{
				var formpath = "<?php echo $formingpath; ?>";

				if(data){
					var path = formpath+data;
					$("#my_images .panel-body").append(`
						<div class = "select-image-item">
							<img class = "img-zoom append-img" data-id = "1" src = "`+path+`">
						</div>
					`)
				}
			}
		});

	}

	function deleteItemGallery($element){

		var path_parts = $element.find('img, source').attr('src').split('/');

		var type;
		var path;
		var dont_delete = 0;

		if( path_parts[path_parts.length-2] == "user-images" ){
			
			type = 2;
			path = path_parts[path_parts.length-2] + "/" + path_parts[path_parts.length-1];

			if( 
				// $element.find('img').attr('src') === $("#chosen_background_image_base_64").val() ||
			 $element.find('img').attr('src') === "<?= $formInfo->selected_image ?>"){
				dont_delete = 1;
			}

		}
		else if( path_parts[path_parts.length-2] == "user-videos" ){

			type = 3;

			var video_path_part = $element.find('source').attr("data-val").split('/');

			path = [ path_parts[path_parts.length-2] + "/" + path_parts[path_parts.length-1], video_path_part[video_path_part.length-2] + "/" + video_path_part[video_path_part.length-1] ]
		
			if( 
				// $element.find('source').attr('data-val') === $("#chosen_video").val() || 
				$element.find('source').attr('data-val').replace("_360", "") === "<?= $formInfo->selected_video ?>"){
				dont_delete = 1;
			}
		
		}
		else{
			dont_delete = 1;
		}

		if(dont_delete){

			$(".container").append(`
				<div class = "tutorial-start-text-box" style = 'width: auto;'> 
					<i class="fas fa-info img-zoom" id = "info_message"></i>
					<h3> <?= __(attention) ?> </h3>
					<p> <?= __(dontDeleteFile) ?> </p> 
					<div class = "tutorial-button-wraper">
						<a class = "btn btn-multiplier btn-green" href = "#" onclick = "clear_tutorial(false);"> <?= __(ok) ?> </a>
					</div>
				</div>`
			);

			return 0;
		}

		$element.remove();
		deleteFile(path, $element.find('img, source').attr("data-id"), type );
	}

	function removeVideo(){

		$("#initial-video").children("video").addClass("hide");
		$("#remove_video").addClass("hidden");
		$("#initial_video_placeholder").removeClass("hide");
		$("#no_video").val(1);

	}

	function deleteFile(path, id, type){

		var form_data = new FormData();

		form_data.append("path", path);
		form_data.append("file_id", id);
		form_data.append("file_type", type);
		form_data.append('form_id', $('#frm_id').val() );

		var url = window.location.origin + "/app/electronicinvitation/deleteFile";

		$.ajax({
			url:url,
			method:"POST",
			data: form_data,
			contentType: false,
			cache: false,
			dataType: "text",
			processData: false,
			beforeSend:function(){
				//$('#loader-show').addClass("loader");
			},
			success:function(data)
			{
				console.log(data);
			}
		});
		
	}

    function readFile(input, callback) {

        if (input.files && input.files[0]) {

			var form_data = new FormData();
			form_data.append("file", input.files[0]);
			form_data.append('form_id',$('#frm_id').val());
			var url = window.location.origin + "/app/electronicinvitation/headerImageUpload";
			$.ajax({
				
				url:url,
				method:"POST",
				data: form_data,
				contentType: false,
				cache: false,
				dataType: "text",
				processData: false,
				beforeSend:function(){
					$(".lds-ellipsis, #uploading_file_text").removeClass("hide");
					$("#confirm_choice_text").addClass("hide");
				},
				success:function(data)
				{
					console.log('video data=>',data);
					var formpath = "<?php echo $formingpath; ?>";

					if(data){
						// $("#header_logo").attr("src", formpath+data);
						// $("#chosen_header_logo_image_base_64").val(formpath+data);

						$("#my_logos .panel-body").append(`
							<div class = "select-image-item logos">
								<img class = "img-zoom append-logo" data-id = "1" src = "`+formpath+data+`">
							</div>
						`)

					}
				
				},
				complete: function(){
					$(".lds-ellipsis, #uploading_file_text").addClass("hide");
					$("#confirm_choice_text").removeClass("hide");
				}
			});
        }
    }

	function deleteLocalImage(delete_icon){

		var $this = $(delete_icon);

		var $image = $this.parent().find('img')
		var image_src = $image.attr("src");
		var holder_src = $("#image_holder").attr("src");
		var chosen_src = $("#chosen_background_image_base_64").val();

		if(image_src == updatedImage){

			$(".container").append(`
				<div class = "tutorial-start-text-box" style = "width: auto;"> 
				<i class="fas fa-info img-zoom" id = "info_message"></i>
				<h3> ${translate.info_text.attention_text[language]} </h3>
				<p> ${translate.button_text.cant_delete_image[language]}  </p> 
				<div class = "tutorial-button-wraper">
					<a class = "btn btn-multiplier" onclick = "clear_tutorial(false);" > ${translate.button_text.afirmative_text[language]} </a>
				</div>
				</div>`
			);

			return false;
		}
    
		var images = JSON.parse( localStorage.getItem("background_images") );
		var localstorage_id = $image.attr("data-localstorage-id");

		if(image_src == holder_src){
		$("#image_holder").attr("src", "");
		}

		images.splice(localstorage_id, 1);
		$this.parent().remove();
		localStorage.setItem("background_images", JSON.stringify(images) );

		return true;
  	}

	function loadLocalImages(){

		var images = JSON.parse( localStorage.getItem("background_images") );

		for(var i = 0; i < images.length; i++){
		
		/*$("#images_preview").append(`
			<div class = "select-image-item">
				<i class="fas fa-trash-alt delete-image-icon"></i>
				<img class = "img-zoom" data-localstorage-id = "${i}" src = "${images[i]}">
			</div>  
		`);*/

		}

		// $(".delete-image-icon").click( function(){
		// 	deleteLocalImage(this)
		// });

		$(".select-image-item img").click(function(){
			$("#image_holder").attr("src", $(this).attr("src") );
			$("#chosen_background_image_base_64").val( $(this).attr("src") );
		});
		
	}

	function loadLocalVideos(){
	
		$(".select-video-tag").click(function(){
			var videopath =  $(this).children("source").attr("src");

			var videoid =  $(this).children("source").attr("data-id");
			var video_360 =  $(this).children("source").attr("data-val");
		
			$("#video_holder_wrapper").html(`
				<video controls>
					<source id = "video_holder" src="`+video_360+`" type="video/mp4">
				</video>
			`)
			
			$("#chosen_video").val(video_360);
			$("#chosen_video_id").val(videoid);
		});

	}

	function createImageData64FromColor(color_hex){

		var canvas = document.getElementById("canvas");
		var ctx = canvas.getContext("2d");
		ctx.fillStyle = color_hex;
		ctx.fillRect(0, 0, canvas.width, canvas.height);

		var base64 = canvas.toDataURL();

		return base64;
	}

  	// Croppie

	var croppie_width = $("#image_holder").css("width");
	var croppie_height = $("#image_holder").css("height") - 50;

	// croppie = new Croppie($("#image_holder_wrapper").get(0), {
	// 	viewport: {
	// 		width: croppie_width,
	// 		height: croppie_height,
	// 		type: 'square',
	// 	},
	// 	boundary: {
	// 		width: croppie_width,
	// 		height: croppie_height,
	// 	},
	// 	enableOrientation: true
	// });

	$("#crop_image").click(function(){

		croppie.bind({
		url: $("#image_holder").attr("src"),
		});

		$("#right_images_controls a").toggleClass("hide");
		$("#image_holder_wrapper div").removeClass("hide");
		$("#image_holder").addClass("hide");

	});

	$('.vanilla-rotate').on('click', function(ev) {
		croppie.rotate(parseInt($(this).data('deg')));
	}); 

  	$("#image_holder_wrapper").get(0).addEventListener('update', function (ev) {
      	croppie.result({type:'base64'}).then(function(resp) {
          croped_result = resp;
    	});
    });

	$("#crop_image_confirm").click(function(){

		$("#image_holder").attr("src", croped_result);
		$("#chosen_image_base_64").val('');

		addImage(croped_result);

		$("#right_images_controls a").toggleClass("hide");
		$("#image_holder").removeClass("hide");
		$("#image_holder_wrapper div").addClass("hide");

	});

	$("#crop_image_cancel").click(function(){

		$("#right_images_controls a").toggleClass("hide");
		$("#image_holder").removeClass("hide");
		$("#image_holder_wrapper div").addClass("hide");

	});	

  	// JS FOR THE TUTORIAL

  	var tutorialFlag = "<?php echo $formInfo->tutorial_watch; ?>";

	if(tutorialFlag == 0){
		
		$(".container").append(`
			<div class = "tutorial-start-text-box"> 
				<h3> <?= __(tutorialStartTitle) ?> </h3>
				<p> <?= __(tutorialStartText) ?> </p> 
				<div class = "tutorial-button-wraper">
					<a class = "btn btn-multiplier btn-green" onclick = "clear_tutorial(false); start_tutorial();" > <?= __(yes) ?> </a>
					<a class = "btn btn-multiplier" onclick = "clear_tutorial()" > <?= __(no) ?> </a>
				</div>
			</div>`
	);
	}
	else{
		clear_tutorial();
	}

	function start_tutorial(){

		var form_data = new FormData();
		form_data.append('form_id',$('#frm_id').val());
		var url = window.location.origin + "/app/electronicinvitation/chnageTutorialSeenFlag";
		$.ajax({
			url:url,
			method:"POST",
			data: form_data,
			contentType: false,
			cache: false,
			dataType: "text",
			processData: false,
			beforeSend:function(){
			//$('#loader-show').addClass("loader");
			},
			success:function(data)
			{
				console.log('video data=>',data);
				if(data){
					$(".tutorial-overlay").removeClass("hide");
					$(".shown").removeClass("shown");

					$(".container").append(`
						<div class = "tutorial-start-text-box" id = "tutorial_welcome_message"> 
							<h3> <?= __(tutorialStartTitle) ?> </h3>
							<p> <?= __(tutorialWelcomeText) ?> </p> 
							<div class = "tutorial-button-wraper">
								<a class = "btn btn-multiplier btn-green" onclick = "tutorial_next_step();" > <?= __(next) ?> </a>
							</div>
						</div>`
					);
				}
			}
		});
	}

	function tutorial_previous_step(){

		var $shown = $(".shown");
		var $this_shown = $( $shown.get( $shown.length - 1) );

		$this_shown.removeClass("shown");

		clear_tutorial(false);

		if( $shown.length === 1 ){
		start_tutorial();
		}
		else{

		var $last_shown = $( $shown.get( $shown.length - 2) );

		$last_shown.removeClass("shown");

		tutorial_show_item( $last_shown );
		}

	}

	function tutorial_next_step(){

		if( $(".tutorial-step").not(".shown").length > 0 ){
			clear_tutorial(false);
			tutorial_show_item( $( $(".tutorial-step").not(".shown").get(0) ) );
		}
		else{
			clear_tutorial();
		}

	}

	function tutorial_show_item($element){

		if( $element.attr("data-tutorial-show-parent") == "false"){
			$(".tutorial-overlay").addClass("tutorial-item-active");
		}
		else{
			$element.parent().addClass("tutorial-item-active");
		}
		
		$element.addClass("shown");

		$element.parent().append(`
			<div class = "tutorial-text-box" style = 'width: auto; min-width: 280px; max-width: 300px;'> 
				<h3> ${$element.attr("data-tutorial-name")} </h3>
				<p> ${$element.attr("data-tutorial-text")} </p> 
				<div class = "tutorial-button-wraper">
				<a class = "btn btn-multiplier" onclick = "tutorial_previous_step();" > <?= __(go_back) ?> </a>
					<a class = "btn btn-multiplier btn-green" onclick = "tutorial_next_step();" > <?= __(next) ?> </a>
				</div>
			</div>`
		);

		var scrollTo = ( $(window).width() > 1000) ? "center" : "start"; 
		console.log(scrollTo);
		console.log($element);
		$element.get(0).scrollIntoView({block: scrollTo, behavior: "smooth"});
	}

	function clear_tutorial(hide_overlay = true){

		if(hide_overlay){
			$(".tutorial-overlay").addClass("hide");
		}

		$(".tutorial-start-text-box").remove();
		$(".tutorial-text-box").remove();
		
		$(".tutorial-item-active").removeClass("tutorial-item-active");

	}

  	//

	function copyToClipboard(elem) {
		// create hidden text element, if it doesn't already exist
		var targetId = "_hiddenCopyText_";
		var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
		var origSelectionStart, origSelectionEnd;
		if (isInput) {
			// can just use the original source element for the selection and copy
			target = elem;
			origSelectionStart = elem.selectionStart;
			origSelectionEnd = elem.selectionEnd;
		} else {
			// must use a temporary form element for the selection and copy
			target = document.getElementById(targetId);
			if (!target) {
				var target = document.createElement("textarea");
				target.style.position = "absolute";
				target.style.left = "-9999px";
				target.style.top = "0";
				target.id = targetId;
				document.body.appendChild(target);
			}
			target.textContent = elem.textContent;
		}
		// select the content
		var currentFocus = document.activeElement;
		target.focus();
		target.setSelectionRange(0, target.value.length);

		// copy the selection
		var succeed;
		try {
			succeed = document.execCommand("copy");
		} catch(e) {
			succeed = false;
		}
		// restore original focus
		if (currentFocus && typeof currentFocus.focus === "function") {
			currentFocus.focus();
		}

		if (isInput) {
			// restore prior selection
			elem.setSelectionRange(origSelectionStart, origSelectionEnd);
		} else {
			// clear temporary content
			target.textContent = "";
		}
		return succeed;
	}

</script>

<script type="text/javascript">

  // PAGE SCRIPTS

    //DEV-116 - Get mandatory aditional data at registration forms
    function countyChanged(){
        var contry_flag = $("#countries_dropdown option:selected").attr("data-flag");
        $('#countryflag').attr('src', contry_flag);

        var country_code = $("#countries_dropdown option:selected").attr("data-phone");
        $('#countrycode').val(country_code);

        var country_id = $("#countries_dropdown option:selected").attr("data-contry-id");

        var states = $("#state");

        if (country_code == '55'){

            $('#phone').inputmask('(99) 99999-9999',{ "clearIncomplete": true });

        }else{
            $('#phone').inputmask('');
            }


        }

    function validateForm(){

        var name = $('#name').val();
        var email = $('#email').val();
        var country = $('#countries_dropdown').val();
        var phone = $('#phone').val();


        if (name == '' || email == '' || country == '' || !validateBRPhone(phone) || !validateEmail(email)){
            return false;
        }else{
            return true;
    }

    }

    function  validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( $email );
    }

    function validateBRPhone($phone) {
        var phoneReg = /(\([1-9][0-9]\)?|[1-9][0-9])\s?([9]{1})([0-9]{4})-?([0-9]{4})$/;
        return phoneReg.test( $phone );
    }

  function fbShareOptionSelect(){
          if($("#share").val()==""){
              $("#shareOptionRequired").show();
                  return false;
          }
          $("#shareOptionRequired").hide();
          $('#social_step_1').hide();$('#social_step_2').show();
  }
  
    var from_data = <?php echo json_encode($form_data) ?>;
    // console.log(from_data);
    var validation_fields = {};
        jQuery.each(from_data, function(i, field)
        {
           if( typeof field.required !== 'undefined' )
           {
                var jsonObj = {};
                if( typeof field.subtype !== 'undefined' )
                {   
                    if( field.subtype == 'email' )
                    {
                        jsonObj['email'] = true;
                    }

                    if( field.subtype == 'tel' )
                    {
                        jsonObj['number'] = '!0';
                        jsonObj['minlength'] = '5';
                        jsonObj['maxlength'] = '10';
                    }
                }
                jsonObj['required'] = true;
                validation_fields[field.name] = jsonObj;
            }
        });

        var flagObj = {};
        flagObj['required'] = true;
        validation_fields['flag'] = flagObj;


        var streamTitleObj = {};
        streamTitleObj['required'] = true;
        validation_fields['stream_title'] = streamTitleObj;


        var streamDescriptionObj = {};
        streamDescriptionObj['required'] = true;
        validation_fields['stream_description'] = streamDescriptionObj;
                
                // console.log(validation_fields);
    var FormValidation = function() {
    var e = function() {
   
    var formLang = $("#form_lang").val();
    //alert(formLang);

      var validMsg = '';
      if(formLang == 'Portuguese')
      {
        validMsg = "Os campos Título e Descrição do post não podem ficar em branco.";
      }
      else if(formLang == 'English')
      {
        validMsg = "Post Title and Description fields must not be blank.";
      }
      else if(formLang == 'Spanish')
      {
        validMsg = "Los campos Título y Descripción de la publicación no deben estar en blanco.";
      }
            var e = $("#form_sample_1"),
                r = $(".alert-danger", e),
                i = $(".alert-success", e);
            e.validate({
                errorElement: "span",
                errorClass: "help-block",
                errorPlacement: function(error, element) {
                    // console.log(element);
                    if( ( element.parent('.mt-radio').length ) || ( element.parent('.mt-checkbox').length) ) {
                        error.insertAfter(element.parent().parent());
                    } else {
                        error.insertAfter(element);
    }
        },
                focusInvalid: !1,
                ignore: "",
                messages: {
                    select_multi: {
                        maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                        minlength: jQuery.validator.format("At least {0} items must be selected")
        },
                    stream_title: validMsg,
                    stream_description: validMsg,
                },
                rules: validation_fields,
                // ,rules: {
                //     name: {
                //         minlength: 2,
                //         required: !0
                //     },
                //     email: {
                //         required: !0,
                //         email: !0
                //     },
                // },
                invalidHandler: function(e, t) {
                    //i.hide(), r.show(), App.scrollTo(r, -200)
                },
                highlight: function(e) {
                    $(e).closest(".form-group").addClass("has-error")
                },
                unhighlight: function(e) {
                    $(e).closest(".form-group").removeClass("has-error")
                },
                success: function(e) {
                    e.closest(".form-group").removeClass("has-error")
                },
                submitHandler: function(e) {
                    return true;
                   // i.show(), r.hide()
                }
    });


            jQuery('#flag').change(function(){
                if( jQuery(this).val() == 'timeline' )
                {
                    jQuery("#share").rules("add", "required");
                    jQuery("#pages").rules("remove", "required");
                    jQuery("#groups").rules("remove", "required");
                    jQuery("#events").rules("remove", "required");

                }
                else if( jQuery(this).val() == 'page' )
                {
                    jQuery("#pages").rules("add", "required");
                    jQuery("#share").rules("remove", "required");
                    jQuery("#groups").rules("remove", "required");
                    jQuery("#events").rules("remove", "required");
                }
                else if( jQuery(this).val() == 'group' )
                {
                    jQuery("#groups").rules("add", "required");
                    jQuery("#pages").rules("remove", "required");
                    jQuery("#share").rules("remove", "required");
                    jQuery("#events").rules("remove", "required");
                }
                else if( jQuery(this).val() == 'event' ){
                    jQuery("#events").rules("add", "required");
                    jQuery("#groups").rules("remove", "required");
                    jQuery("#pages").rules("remove", "required");
                    jQuery("#share").rules("remove", "required");
                }
                else
                {

                }
            });

        };
    return {
        init: function() {
            e() // t(), r(), i()
        }
    }
	}();
  jQuery(document).ready(function() {


        //DEV-116 - Get mandatory aditional data at registration forms
        $('#countries_dropdown').val('br'); 
        $('#countries_dropdown').trigger('change'); 

    $('#change_user').click(function(event){
        event.preventDefault();
        var urlLogout = base_url + 'commingusers/fblogout/?id=';

        <?php  if($session->read( 'social.is_login' ) == "google"){ ?>
            urlLogout = base_url + 'commingusers/commonLogOut/?id=';
        <?php } ?>
        var logout_url =  $(this).attr('href');
        $.ajax({
            'url' : urlLogout + <?php echo $_GET['id'];?>,
            success: function(data){
               window.location.href = logout_url;
            }
        });

    });
    $('#flag').change(function(){
        var flag = $('#flag').val();
        if(flag == 'timeline'){
           
            var hide_share_option = '<?php echo $FormListingData['hide_share_option'];?>';
            if( hide_share_option == "0"){
               $('#show_timeline').show();
            }
            $('#stream_title').hide();
            $('#show_pages').hide();
            $('#show_events').hide();
            $('#show_groups').hide();
        } else if(flag == 'page') {
            $('#show_pages').show();
            $('#show_events').hide();
            $('#stream_title').hide();
            $('#show_timeline').hide();
            $('#show_groups').hide();
        } else if(flag == 'group') {
            $('#show_groups').show();
            $('#show_events').hide();
            $('#show_pages').hide();
            $('#stream_title').hide();
            $('#show_timeline').hide();
        }else if(flag == 'event'){
            $('#show_events').show();
            $('#show_groups').hide();
            $('#show_pages').hide();
            $('#stream_title').hide();
            $('#show_timeline').hide();
        }else{
            $('#show_pages').hide();
            $('#show_events').hide();
            $('#show_timeline').hide();
            $('#stream_title').hide();
            $('#show_groups').hide();
            return false;
        }
    });
    $('#get_values').click(function(){
        window.location.href = base_url + 'commingusers/fblogin?id=' + <?php echo $_GET['id'];?>; 
    });

    $('#get_values_gmail').click(function(){

            if (validateForm() == true){

                //Get values from form to session (AJAX)
                var name_value = $('#name').val();
                var email_value = $('#email').val();
                var country_value = $('#countries_dropdown option:selected').text();
                var phone_value = $('#phone').val();
                var state_value = $('#state option:selected').text();
                var city_value = $('#city option:selected').text();


                $.ajax({
                    url: "/app/commingusers/saveFormSession",
                    type: "POST",
                    data: {name: name_value, email: email_value, country: country_value, phone: phone_value, state: state_value, city: city_value },
                    dataType: "json"
                }).done(function(result) {
  
    });


        window.location.href = base_url + 'commingusers/glogin?id=' + <?php echo $_GET['id'];?>; 
            }else{

                var formLang = $("#form_lang").val();
                if(validateBRPhone(phone))
                {
                    if(formLang == 'Portuguese')
                {
                    var validMsg = "Todos os campos são de preenchimento obrigatório.";
                }
                else if(formLang == 'English')
                {
                    var validMsg = "All fields are required.";
                }
                else if(formLang == 'Spanish')
                {
                    var validMsg = "Todos los campos son obligatorios.";
                }
                alert(validMsg);

               }
               else
               {
                if(formLang == 'Portuguese')
                {
                    var validMsg = "Número de telefone móvel inválido.";
                }
                else if(formLang == 'English')
                {
                    var validMsg = "Invalid mobile phone number.";
                }
                else if(formLang == 'Spanish')
                {
                    var validMsg = "Número de teléfono móvil no válido.";
                }
                $('#phone').blur();
                alert(validMsg);

               }
           }
  });


        $('#get_values_facebook').click(function(){

            if (validateForm() == true){

                //Get values from form to session (AJAX)
                var name_value = $('#name').val();
                var email_value = $('#email').val();
                var country_value = $('#countries_dropdown option:selected').text();
                var phone_value = $('#phone').val();



                $.ajax({
                    url: "/app/commingusers/saveFormSession",
                    type: "POST",
                    data: {name: name_value, email: email_value, country: country_value, phone: phone_value },
                    dataType: "json"
                }).done(function(result) {

                });


                $('.fb_login_box').show();
                $('#col_social_text, #col_social_form, .flag_row, .img_row').hide();
                // $('html, body').animate({ scrollTop: 0 }, 'slow');



    }else{

		var formLang = $("#form_lang").val();
		if(validateBRPhone(phone))
		{
			if(formLang == 'Portuguese')
		{
			var validMsg = "Todos os campos são de preenchimento obrigatório.";
			}
		else if(formLang == 'English')
		{
			var validMsg = "All fields are required.";
		}
		else if(formLang == 'Spanish')
		{
			var validMsg = "Todos los campos son obligatorios.";
		}
		alert(validMsg);

		}
		else
		{
		if(formLang == 'Portuguese')
		{
			var validMsg = "Número de telefone móvel inválido.";
		}
		else if(formLang == 'English')
		{
			var validMsg = "Invalid mobile phone number.";
		}
		else if(formLang == 'Spanish')
		{
			var validMsg = "Número de teléfono móvil no válido.";
		}
		alert(validMsg);

		}
	}
    });
   
  FormValidation.init();
   
  });

</script>

<script type="text/javascript">

	$(document).on('change', '.other', function (event) {
		if($(this).is(':checked')){
			$(".other-box").show(200);
			$(".other-box").prop('required',true);
			// $(this).parent().find(".help-block").show();
		}else{
			$(".other-box").hide(200);
			// $(this).parent().find(".help-block").hide();
			$(".other-box").prop('required',false);
		}
	});

	$(document).on('keyup', '.other-box', function (event) {
		$(".other").val($(this).val());
	});

	$(document).on("click", "input[class='radio-input']", function (event) 
	{
		var is_other = $(this).attr('other-field');

		if(is_other == '1'){
			$(".other-box-radio").show(200);
			$(".other-box-radio").prop('required',true);
			// $(this).parent().find(".help-block").show();
		}else{
			$(".other-box-radio").hide(200);
			// $(this).parent().find(".help-block").hide();
			$(".other-box-radio").prop('required',false);
	}

	});
	$(document).on('keyup', '.other-box-radio', function (event) {
		$(".radio-input").val($(this).val());
	});

</script>

<script>

	$(document).on('change', '.addvideogallary_video_input', function(e){
		//alert();
		var form_data = new FormData();
		form_data.append("file", document.getElementById('addvideogallary').files[0]);
		form_data.append('form_id',$('#frm_id').val());
		var url = window.location.origin + "/app/electronicinvitation/staticVideoUpload";
		$.ajax({	
			url:url,
			method:"POST",
			data: form_data,
			contentType: false,
			cache: false,
			dataType: "text",
			processData: false,
			beforeSend:function(){
				$(".lds-ellipsis, #uploading_file_text").removeClass("hide");
				$("#confirm_choice_text").addClass("hide");
			},
			success:function(dt)
			{
				console.log('video data=>',dt);
				data = JSON.parse(dt);
				if(data){
					var thumb_path = "<?php echo $dynamicvideopath; ?>"+data.videoName_thumb;
					var video_id = data.videoName_id;
					var video_360 = "<?php echo $dynamicvideopath; ?>"+data.videoName_360;

					$("#my_videos .panel-body").append(`
						<div class = "select-image-item videos">
							<video class = "img-zoom append-video">
								<source  src="`+thumb_path+`"  data-id="`+video_id+`"  data-val="`+video_360+`" type="video/mp4">
							</video>
						</div>
					`)
				}
			},
			complete: function(){
				$(".lds-ellipsis, #uploading_file_text").addClass("hide");
				$("#confirm_choice_text").removeClass("hide");
			}
			});
	});

</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-150918709-1"></script>

<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-150918709-1');
</script>

<style>
/** LOADER */

	.lds-ellipsis {
	display: inline-block;
	position: relative;
	width: 80px;
	height: 10px;
	}
	.lds-ellipsis div {
	position: absolute;
	top: 0px;
	width: 8px;
	height: 8px;
	border-radius: 50%;
	background: #fff;
	animation-timing-function: cubic-bezier(0, 1, 1, 0);
	}
	.lds-ellipsis div:nth-child(1) {
	left: 8px;
	animation: lds-ellipsis1 0.6s infinite;
	}
	.lds-ellipsis div:nth-child(2) {
	left: 8px;
	animation: lds-ellipsis2 0.6s infinite;
	}
	.lds-ellipsis div:nth-child(3) {
	left: 32px;
	animation: lds-ellipsis2 0.6s infinite;
	}
	.lds-ellipsis div:nth-child(4) {
	left: 56px;
	animation: lds-ellipsis3 0.6s infinite;
	}
	@keyframes lds-ellipsis1 {
	0% {
		transform: scale(0);
	}
	100% {
		transform: scale(1);
	}
	}
	@keyframes lds-ellipsis3 {
	0% {
		transform: scale(1);
	}
	100% {
		transform: scale(0);
	}
	}
	@keyframes lds-ellipsis2 {
	0% {
		transform: translate(0, 0);
	}
	100% {
		transform: translate(24px, 0);
	}
	}

</style>